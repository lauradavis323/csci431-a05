package advisorAssignment;

public class Student {
	private String firstName, lastName;
	
	public Student(String first, String last){
		this.firstName = first;
		this.lastName = last;
	}
	
	public String getFirstName(){
		return firstName;
	}
	public String getLastName(){
		return lastName;
	}
}
