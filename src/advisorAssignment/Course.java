package advisorAssignment;

public class Course {
	private String courseName;
	public String id;
	public boolean offeredNextSemester;
	
	public Course(String id, String name, boolean offered){
		this.courseName = name;
		this.id = id;
		this.offeredNextSemester = offered;
	}
	
	public String getName(){
		return courseName;
	}
	public String getID(){
		return id;
	}
	public boolean offered(){
		return offeredNextSemester;
	}
}
