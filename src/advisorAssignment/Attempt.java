package advisorAssignment;

public class Attempt {
	public Student s;
	public Course c;
	public boolean p;
	
	public Attempt(Student s, Course c, boolean p){
		this.s = s;
		this.c = c;
		this.p = p;
	}
	
	public Student getStudent(){
		return s;
	}
	public Course getCourse(){
		return c;
	}
	public boolean passedForPrereq(){
		return p;
	}
}
