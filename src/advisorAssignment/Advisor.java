package advisorAssignment;

import java.util.Scanner;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieContainer;
import org.kie.api.runtime.KieSession;

public class Advisor {
	public static void main(String[] args){
		KieContainer kc = KieServices.Factory.get().getKieClasspathContainer();
		KieSession ksession = kc.newKieSession("AdvisorProjectKS");
		
		Scanner scanner = new Scanner(System.in);
		String lineSeparator = System.getProperty("line.separator");
		scanner.useDelimiter(lineSeparator);
		
		String firstName, lastName;
		System.out.println("Student what is your first name?");
		firstName = scanner.next();
		System.out.println("Student what is your last name?");
		lastName = scanner.next();
		Student sa = new Student(firstName, lastName);
		
		Course ca = new Course("CSCI141", "Introduction to Computer Science I", true);
		Course cb = new Course("CSCI142", "Introduction to Computer Science II", true);
		Course cc = new Course("CSCI201", "Introduction to Computer Organization", false);
		Course cd = new Course("CSCI211", "Discrete Structures", true);
		Course ce = new Course("CSCI221", "Software Development I", true);
		Course cf = new Course("CSCI321", "Software Development II", false);
		Course cg = new Course("CSCI301", "Operating Systems", false);
		Course ch = new Course("CSCI311", "Algotithm Analysis", false);
		
		Attempt aca, acb, acc, acd, ace;
		
		String answer;
		System.out.println("Did you take intro to comp sci 1?");
		answer = scanner.next();
		if(answer.equalsIgnoreCase("yes")){
			System.out.println("Did you pass?");
			answer = scanner.next();
			if(answer.equalsIgnoreCase("yes")){
				aca = new Attempt(sa, ca, true);
			}
			else {
				aca = new Attempt(sa, ca, false);
			}
		}
		else {
			aca = null;
		}
		System.out.println("Did you take intro to comp sci 2");
		answer = scanner.next();
		if(answer.equalsIgnoreCase("yes")){
			System.out.println("Did you pass?");
			answer = scanner.next();
			if(answer.equalsIgnoreCase("yes")){
				acb = new Attempt(sa, cb, true);
			}
			else {
				acb = new Attempt(sa, cb, false);
			}
		}
		else {
			acb = null;
		}
		System.out.println("Did you take Introduction to Computer Organization?");
		answer = scanner.next();
		if(answer.equalsIgnoreCase("yes")){
			System.out.println("Did you pass?");
			answer = scanner.next();
			if(answer.equalsIgnoreCase("yes")){
				acc = new Attempt(sa, cc, true);
			}
			else {
				acc = new Attempt(sa, cc, false);
			}
		}
		else {
			acc = null;
		}
		System.out.println("Did you take Discrete Structures?");
		answer = scanner.next();
		if(answer.equalsIgnoreCase("yes")){
			System.out.println("Did you pass?");
			answer = scanner.next();
			if(answer.equalsIgnoreCase("yes")){
				acd = new Attempt(sa, cd, true);
			}
			else {
				acd = new Attempt(sa, cd, false);
			}
		}
		else {
			acd = null;
		}
		System.out.println("Did you take Software Development 1?");
		answer = scanner.next();
		if(answer.equalsIgnoreCase("yes")){
			System.out.println("Did you pass?");
			answer = scanner.next();
			if(answer.equalsIgnoreCase("yes")){
				ace = new Attempt(sa, ce, true);
			}
			else {
				ace = new Attempt(sa, ce, false);
			}
		}
		else {
			ace = null;
		}
		
		ksession.insert(sa);
		
		ksession.insert(ca);
		ksession.insert(cb);
		ksession.insert(cc);
		ksession.insert(cd);
		ksession.insert(ce);
		ksession.insert(cf);
		ksession.insert(cg);
		ksession.insert(ch);
		
		if(aca != null)
			ksession.insert(aca);
		if(acb != null)
			ksession.insert(acb);
		if(acc != null)
			ksession.insert(acc);
		if(acd != null)
			ksession.insert(acd);
		if(ace != null)
			ksession.insert(ace);
		
		System.out.println("The following will be the classes you are qualified to take and whether they are offered next semester.");
		
		ksession.fireAllRules();
	}
}
